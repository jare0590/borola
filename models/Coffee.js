var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Coffee Model
 * =============
 */

var Coffee = new keystone.List('Coffee', {
	label: 'Café',
	singular: 'Café',
	plural: 'Cafes',
	autokey: {from: 'name', path: 'slug', unique: true}
});

Coffee.add({
	name: { type: Types.Text, required: true, initial: true },
	extract: { type: Types.Textarea, required: false, initial: true },
	description: { type: Types.Textarea, required: false, initial: false },
	intensity : { type: Types.Select, required: false, options: [
			{value: '0', label: 'Ligero'},
			{value: '1', label: 'Ligero-Medio'},
			{value: '2', label: 'Medio'},
			{value: '3', label: 'Medio-Intenso'},
			{value: '4', label: 'Intenso'},
	]},
	country: { type: Types.Relationship, ref: 'Country', required: true, initial: true },
	state: { type: Types.Relationship, ref: 'State', required: true, initial: true },
	origin: { type: Types.Relationship, ref: 'Origin', required: false, initial: false },
	kind: { type: Types.Relationship, ref: 'Kind', required: true, initial: true },
	process: { type: Types.Relationship, ref: 'Process', required: true, initial: true },
	image: { type: Types.CloudinaryImage, autoCleanup: true}
});

/**
 * Registration
 */

Coffee.defaultColumns = 'name, country, state, intensity';
Coffee.register();
