var keystone = require('keystone');
var Types 	 = keystone.Field.Types;

/**
 *  User_Plan Model
 *  ===============
 */

var User_Plan = new keystone.List('User_Plan', {
	label: 'Plan de Usuario',
	singular: 'Plan de Usuario',
	plural: 'Planes de Usuarios',
	autokey: {from: 'user', path: 'slug', unique: true },
});

User_Plan.add({
	user: { type: Types.Relationship, ref: 'User', required: true, initial: true },
	plan: { type: Types.Relationship, ref: 'Plan', required: true, initial: true },
	coffee_package: { type: Types.Relationship, ref: 'Coffee_Package', many: true, required: true, initial: true },
	molienda: { type: Types.Relationship, ref: 'Molienda', required: true, initial: true }
});

/**
 * Registration
 */

User_Plan.defaultColumns = 'user, plan, coffee_package';
User_Plan.register();
