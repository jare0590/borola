var keystone = require('keystone');
var Types 	 = keystone.Field.Types;

/**
 *  Plan Model
 *  ==========
 */

var Plan = new keystone.List('Plan', {
	label: 'Plan',
	singular: 'Plan',
	plural: 'Planes',
	autokey: {from: 'name', path: 'slug', unique: true},
});

Plan.add({
	name: { type: Types.Text, required: true, initial: true },
	price: { type: Types.Money, required: true, initial: true, foramat: '$0,0.00' },
	currency: { type: Types.Text, required: false, initial: false, default: 'MXN', noedit: true },
	trial_days: { type: Types.Number, required: false, initial: false, default: '0' },
	package_per_month: { type: Types.Number, required: true, initial: true, default: 1 },
	cobro: { type: Types.Select, required: true, options: [
			{value: 'week', label: 'Semanal'},
			{value: 'half_month', label: 'Quincenal'},
			{value: 'month', label: 'Mensual'},
			{value: 'year', label: 'Anual'}
	], default: 'month' },
	photo: { type: Types.CloudinaryImage, autoCleanup: true},
	videos: { type: Types.Number, required: true, initial: true},
	weight: { type: Types.Relationship, ref: 'Weight', required: true, initial: true, index: true }
});

/**
 * Registration
 */

Plan.defaultColumns = 'name, package_per_month, price';
Plan.register();
