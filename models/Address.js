var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Address Model
 * =============
 */

var Address = new keystone.List('Address', {
	label: 'Dirección',
	singular: 'Dirección',
	plural: 'Direcciones',
	autokey: {from: 'street1', path: 'slug', unique: true}
});

Address.add({
	user: { type: Types.Relationship, ref: 'User', required: true, initial: true },
	zip: { type: Types.Number, required: true, initial: true },
	suburb : { type: Types.Text, required: true, initial: true },
	street1: { type: Types.Text, required: true, initial: true },
	street2: { type: Types.Text, initial: true },
	city: { type: Types.Text, required: true, initial: true },
	country: { type: Types.Relationship, ref: 'Country', required: true, initial: true },
	state: { type: Types.Relationship, ref: 'State', required: true, initial: true },
});

/**
 * Registration
 */

Address.defaultColumns = 'user, suburb, street1, city, zip';
Address.register();
