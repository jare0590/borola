var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Kind Model
 * =============
 */

var Kind = new keystone.List('Kind', {
	label: 'Especie de Café',
	singular: 'Especie',
	plural: 'Especies',
	autokey: {from: 'name', path: 'slug', unique: true}
});

Kind.add({
	name: { type: Types.Text, required: true },
});

/**
 * Registration
 */

Kind.defaultColumns = 'name';
Kind.register();
