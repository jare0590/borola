// var keystone = require('keystone');
// var Types = keystone.Field.Types;

// /**
//  * Plan_Interval Model
//  * =============
//  */

// var Plan_Interval = new keystone.List('Plan_Interval', {
// 	label: 'Intervalo del Plan',
// 	singular: 'Intervalo del Plan',
// 	plural: 'Intervalos de Planes'
// });

// Plan_Interval.add({
// 	name: { type: Types.Select, required: true, options: [
// 			{value: 'week', label: 'Semanal'},
// 			{value: 'half_month', label: 'Quincenal'},
// 			{value: 'month', label: 'Mensual'},
// 			{value: 'year', label: 'Anual'}
// 	]},
// });

// /**
//  * Registration
//  */

// Plan_Interval.register();
