var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Country Model
 * =============
 */

var Country = new keystone.List('Country', {
	label: 'País',
	singular: 'País',
	plural: 'Países',
	autokey: {from: 'name', path: 'slug', unique: true}
});

Country.add({
	name: { type: Types.Text, required: true },
});

/**
 * Registration
 */

Country.defaultColumns = 'name';
Country.register();
