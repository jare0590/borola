var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Process Model
 * =============
 */

var Process = new keystone.List('Process', {
	label: 'Proceso',
	singular: 'Proceso',
	plural: 'Procesos',
	autokey: {from: 'name', path: 'slug', unique: true}
});

Process.add({
	name: { type: Types.Text, required: true },
});

/**
 * Registration
 */

Process.defaultColumns = 'name';
Process.register();
