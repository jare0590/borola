var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * State Model
 * =============
 */

var State = new keystone.List('State', {
	label: 'Estado',
	singular: 'Estado',
	plural: 'Estados',
	autokey: {from: 'name', path: 'slug', unique: true}
});

State.add({
	name: { type: Types.Text, required: true },
});

/**
 * Registration
 */

State.defaultColumns = 'name';
State.register();
