var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Molienda Model
 * =============
 */

var Molienda = new keystone.List('Molienda', {
	label: 'Molienda de Café',
	singular: 'Molienda',
	plural: 'Moliendas',
	autokey: {from: 'name', path: 'slug', unique: true}
});

Molienda.add({
	name: { type: Types.Text, required: true },
});

/**
 * Registration
 */

Molienda.defaultColumns = 'name';
Molienda.register();
