var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Coffee_Package Model
 * ====================
 */

var Coffee_Package = new keystone.List('Coffee_Package', {
	label: 'Paquete de Café',
	singular: 'Paquete de Café',
	plural: 'Paquetes de Cafes',
	autokey: {from: 'name', path: 'slug', unique: true}
});

Coffee_Package.add({
	name: { type: Types.Text, required: true, initial: true },
	description: { type: Types.Textarea, required: false, initial: false },
	price : { type: Types.Money, required: true, format: '$0,0.00', initial: true },
	weight: { type: Types.Relationship, ref: 'Weight', required: true, initial: true },
	coffee: { type: Types.Relationship, ref: 'Coffee', required: true, initial: true },
});

/**
 * Registration
 */

Coffee_Package.defaultColumns = 'name, coffee, weight, price';
Coffee_Package.register();
