var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Weight Model
 * =============
 */

var Weight = new keystone.List('Weight', {
	label: 'Peso en gramos',
	singular: 'Peso',
	plural: 'Pesos',
	autokey: {from: 'weight', path: 'slug', unique: true},
	map: {name: 'weight' }
});

Weight.add({
	weight: { type: Types.Number, required: true, initial: true },
});

Weight.relationship({
	ref: 'Plan',
	refPath: 'weight' 
});

/**
 * Registration
 */

Weight.defaultColumns = 'weight';
Weight.register();
