var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Origin Model
 * =============
 */

var Origin = new keystone.List('Origin', {
	label: 'Origen del Café (Pueblo, Comunidad, Ciudad)',
	singular: 'Origen',
	plural: 'Origenes',
	autokey: {from: 'name', path: 'slug', unique: true}
});

Origin.add({
	name: { type: Types.Text, required: true },
});

/**
 * Registration
 */

Origin.defaultColumns = 'name';
Origin.register();
