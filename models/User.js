var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * User Model
 * ==========
 */

var User = new keystone.List('User', {
	label: 'Usuario',
	singular: 'Usuario',
	plural: 'Usuarios',
	autokey: {from: 'email', path: 'slug', unique: true},
});

User.add({
	email: { type: Types.Email, initial: true, required: true, unique: true, index: true },
	name: { type: Types.Name, required: true, index: true },
	password: { type: Types.Password, initial: true, required: true },
	phone: { type: Types.Number, initial: false, required: false },
	cell_phone: { type: Types.Number, initial: true, required: true, default: 0 },
	corporate: { type: Types.Boolean, initial: false, required: false, default: false },
	address: { type: Types.Relationship, ref: 'Address', many: true, required: false },
	favorite_coffee: { type: Types.Relationship, ref: 'Coffee', required: false },
}, 'Permissions', {
	isAdmin: { type: Boolean, label: 'Can access Keystone', index: true },
});

// Provide access to Keystone
User.schema.virtual('canAccessKeystone').get(function () {
	return this.isAdmin;
});


/**
 * Registration
 */
User.defaultColumns = 'name, email, isAdmin';
User.register();
