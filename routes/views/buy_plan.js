var keystone = require('keystone');
var Plan 	 = keystone.list('Plan')

exports = module.exports = function (req, res) {
	var plan = req.query.plan;

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'buy_plan';

	// Load the galleries by sortOrder
	view.query('plan',
		keystone.list('Plan').model.find()
			.where('_id', plan)
			.populate('weight')
	);
	
	view.query('coffees',
		keystone.list('Coffee').model.find()
			.populate('process')
			.populate('kind')
			.populate('state')
			.populate('origin')
	);

	// Render the view
	view.render('buy_plan');

};
