var keystone = require('keystone');

exports = module.exports = function(req, res) {
	var view   = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'planes';

	// Load Products
	view.query('plans', keystone.list('Plan').model.find());

	// Render View
	view.render('planes');
}