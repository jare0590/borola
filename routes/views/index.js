var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'home';

	// Load Products
	view.query(
		'plans',
		keystone.list('Plan').model.find()
			.populate('weight'));

	view.query('coffees', keystone.list('Coffee').model.find());

	// Render the view
	view.render('index');
};
