var gulp       = require('gulp'),
    sass       = require('gulp-sass'),
    image      = require('gulp-imagemin'),
    uglify     = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    watch 	   = require('gulp-watch'),
    pump 	   = require('pump');

gulp.task('eligeSedeUgly', function() {
  return gulp.src([
  		'!public/js/bootstrap/**/*.js',
        'public/js/**/*.js'
    ])
    .pipe(uglify('eligeSedeUgly.min.js', {
      outSourceMap: true
    }))
    .pipe(gulp.dest('sitio/static/js/templates/reservacion'));
});

gulp.task('images', function() {
  gulp.src(['sitio/static/img/*'])
    .pipe(image())
    .pipe(gulp.dest('sitio/static/img'))
});

gulp.task('styles', function () {
    return gulp.src([
    		'!public/styles/bootstrap/**/*.scss',
    		'public/styles/**/*.scss'
    	])
        .pipe(sourcemaps.init())
        .pipe(sass({
        	outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('public/styles'));
});

//watch task
gulp.task('watch', function() {
    gulp.watch([
    	'!public/styles/bootstrap/**/*',
    	'public/styles/**/*.scss'
    ], ['styles']);
    //gulp.watch(['public/js/**/*.js'], ['uglify']);
});

gulp.task('default',['styles', 'watch']);

gulp.task('imgOptimization',['images']);

gulp.task('uglify',['baseUgly']);
